package edu.sabanciuniv.APItest;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Hello App!
 *
 */
public class App {
	
	private static final String INPUT_FILE_NAME = "usernames.txt";
	private static final String OUTPUT_FILE_NAME = "instagram.txt";
	
	public static void main(String[] args) throws IOException {
			
			int totalSearch = 0;
			int success = 0;
			int fail = 0; // if user not available.
			int connectionReset = 0;
			
			String line;
			ArrayList<String> usernames = new ArrayList<>();
	
			/*
			 * get usernames from file and put into username array.
			 */
			try (
			    InputStream is = new FileInputStream(INPUT_FILE_NAME);
			    InputStreamReader isr = new InputStreamReader(is, Charset.forName("UTF-8"));
			    BufferedReader br = new BufferedReader(isr);
			) {
			    while ((line = br.readLine()) != null) {
			    	String[] words = line.split(" ");
				    for(int i = 0; i < words.length; i++){
				    	usernames.add(words[i]);
				    }
			    }
			}
			
			PrintWriter writer = new PrintWriter(OUTPUT_FILE_NAME, "UTF-8");
			
			/*
			 * for each username, reach their data and fetch it.
			 */
			for(int i = 0; i < usernames.size(); i++){
				try {
					//create user folder
					File userPath = new File("img/"+usernames.get(i)+"/");
					
					if(!userPath.exists())
						userPath.mkdirs();
					
					totalSearch++;
					URL instagram = new URL("https://www.instagram.com/"+usernames.get(i)+"/media");
					
					BufferedReader in = new BufferedReader(
					   new InputStreamReader(instagram.openStream()));
					
					String jsonString = "";
					String inputLine;
			     	while ((inputLine = in.readLine()) != null){
			     		jsonString += inputLine;
			     	}
			     	writer.println(usernames.get(i));
					writer.println("-------------------------------------");
					//writer.println(jsonString);
			     	JSONObject jsonObject = new JSONObject(jsonString);
		     		JSONArray jsonArray = (JSONArray) jsonObject.get("items");
		     		
		     		for(int k = 0; k < jsonArray.length(); k++){
		     			JSONObject object = (JSONObject) jsonArray.get(k);
		     			String standartResolutionImageURL = 
		     					object.getJSONObject("images").
		     					getJSONObject("standard_resolution").getString("url");
		     			writer.println(standartResolutionImageURL);
		     			
		     			String created_time_str = object.getString("created_time");
		     			long created_time = Long.parseLong(created_time_str+"000"); 
		     			//read image
		     			URL imgURL = new URL(standartResolutionImageURL);
						HttpsURLConnection connection = (HttpsURLConnection) imgURL.openConnection();
						connection.addRequestProperty("User-Agent", "");
						BufferedImage img = ImageIO.read(connection.getInputStream());
						
						// draw the image
						Graphics2D g = (Graphics2D) img.getGraphics();
						g.drawImage(img, 0, 0, null);

						// save as new image
						Date d = new Date(created_time);
						ImageIO.write(img, "JPG", new File(userPath, String.format("%02d", k+1) + " - " + d.toString().replace(':', '.') + ".jpg"));
						
						g.dispose();
		     		}
			     		
			     	System.out.println(i + " is finished!");
			     	writer.println();
			     	success++;
			     	in.close();
				} catch (Exception e) {
					String msg = e.getMessage();
					System.out.println(msg);
					if(msg.equals("Connection reset")){
						connectionReset++;
					}else{
						fail++;
					}
					//e.printStackTrace();
				}
			}
			writer.close();
			System.out.println("Total search: " + totalSearch);
			System.out.println("Success search: " + success);
			System.out.println("Connection Reset search: " + connectionReset);
			System.out.println("Fail search: " + fail);
		}
}
